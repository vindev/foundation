package us.vindere.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import us.vindere.core.core;

public class fVersion implements CommandExecutor {

    private core plugin;
    public fVersion(core pl) {
        plugin = pl;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (command.getName().equalsIgnoreCase("fversion")) {
            sender.sendMessage(plugin.color.codes("&8[&fFoundation Core&8] &fVersion: &7" + plugin.getDescription().getVersion()));
            sender.sendMessage(plugin.color.codes("&7" + plugin.getDescription().getDescription()));
            sender.sendMessage(plugin.color.codes("&fDeveloped By: &7" + plugin.getDescription().getAuthors()));
            sender.sendMessage(plugin.color.codes("&fWebsite: &7" + plugin.getDescription().getWebsite()));
            return true;
        }
        return false;
    }
}
