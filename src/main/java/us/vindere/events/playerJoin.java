package us.vindere.events;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.Permission;
import us.vindere.core.core;

import java.util.ArrayList;
import java.util.Set;

public class playerJoin implements Listener {
    // Instance plugin.
    private core plugin;
    public playerJoin(core pl){
        plugin = pl;
    }

    // Runs whenever a player joins.
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        ArrayList<String> motdFormat = new ArrayList<String>();
        ArrayList<Permission> permissions = new ArrayList<Permission>();
        int count;

        // Checks to see which database system the plugin is configured for and will look up or create an entry for
        // the player if they do not have one already.
        if (plugin.config.getPluginConfig().getString("database_type").equals("mongodb")) {
            MongoCollection<Document> playerCollection = plugin.playerCollection;
            Document playerRecord = new Document("uuid", player.getUniqueId().toString());
            Document playerFound = plugin.playerData.getDocument(player);

            Integer starting_bal = plugin.config.getPluginConfig().getInt("economy.starting_balance");
            String date = plugin.date.getDate("short");
            long total = playerCollection.count();

            if (playerFound == null) {
                playerRecord.append("player", player.getName());
                playerRecord.append("display_name", player.getName());
                playerRecord.append("balance", starting_bal);
                playerRecord.append("ip", player.getAddress().getHostString());
                playerRecord.append("first_login", date);
                playerRecord.append("last_login", date);
                playerCollection.insertOne(playerRecord);
            } else {
                String display_name = playerFound.getString("display_name");
                Bson updateIP = new Document("ip", player.getAddress().getHostString());
                Bson updateDate = new Document("last_login", date);

                Bson updateIPOperation = new Document("$set", updateIP);
                Bson updateDateOperation = new Document("$set", updateDate);
                playerCollection.updateOne(playerFound, updateIPOperation);
                playerCollection.updateOne(playerFound, updateDateOperation);

                player.setDisplayName(plugin.color.codes(display_name + "&r"));
            }
        }

        for (String key : plugin.config.getMOTDConfig().getConfigurationSection("motd").getKeys(true)) {
             plugin.console.sendMsg("t", key);
             motdFormat.add(key);
        }

        for (count = 0; count < motdFormat.size(); count++) {
            Permission permission = new Permission("core.motd." + motdFormat.get(count));
            if (player.hasPermission(permission)) {
                break;
            }
        }

        plugin.console.sendMsg("t", Integer.toString(count));

    }
}
