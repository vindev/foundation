package us.vindere.core;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.LoggerFactory;
import us.vindere.cofig.configLoader;
import us.vindere.command.fVersion;
import us.vindere.events.playerJoin;
import us.vindere.util.cons;
import us.vindere.util.database.mongoDB;
import us.vindere.util.date;
import us.vindere.util.playerData;
import us.vindere.util.trans;

public final class core extends JavaPlugin {

    public cons console = new cons(this);
    public trans color = new trans(this);
    public date date = new date(this);

    public configLoader config = new configLoader(this);
    public mongoDB mongodb = new mongoDB(this);
    public playerData playerData = new playerData(this);

    public MongoCollection<Document> playerCollection;

    @Override
    public void onEnable() {
        ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("org.mongodb.driver").setLevel(Level.ERROR);

        // Loads all configurations.
        config.loadAllConfigs();

        this.getCommand("fversion").setExecutor(new fVersion(this));

        setEvents();

        this.playerCollection = mongodb.getPlayerCollection();
    }

    @Override
    public void onDisable() { }

    // Registers events for use.
    private void setEvents() {
        playerJoin playerJoin = new playerJoin(this);
        Bukkit.getPluginManager().registerEvents(playerJoin, this);
    }
}
