package us.vindere.cofig;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import us.vindere.core.core;

import java.io.File;
import java.io.IOException;

public class configLoader {
    // Assigns null values to variables required later to load configuration files.
    private File plFile;
    private FileConfiguration plConfig;
    private File msgFile;
    private FileConfiguration msgConfig;
    private File dbFile;
    private FileConfiguration dbConfig;
    private File motdFile;
    private FileConfiguration motdConfig;

    // Instance plugin.
    private core plugin;
    public configLoader(core pl) {
        plugin = pl;
    }

    // Load all configuration files.
    public void loadAllConfigs() {
        this.loadPluginConfig();
        this.loadMsgConfig();
        this.loadMOTDConfig();
        this.loadDBConfig();
    }


    // Plugin Configuration
    // Path    -    config.yml
    // Purpose -    To allow customization and further expand the possibilities of use for the core plugin.

    // Gets plugin configuration and returns as a FileConfiguration object.
    public FileConfiguration getPluginConfig() {
        return this.plConfig;
    }

    // Loads 'config.yml' to the server.
    public void loadPluginConfig() {
        plFile = new File(plugin.getDataFolder(), "config.yml");

        // Checks to see if 'config.yml' exists.
        if(!plFile.exists()) {
            plFile.getParentFile().mkdirs();
            plugin.saveResource("config.yml", false);
            plugin.console.sendMsg("i","Main configuration not found, creating 'config.yml'");
        } else {
            plugin.console.sendMsg("i", "Main configuration found, loading 'config.yml'");
        }

        // Sets variable 'plConfig' as a YamlConfiguration and loads it.
        plConfig = new YamlConfiguration();
        try {
            plConfig.load(plFile);
        } catch(IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }


    // Messsages Configuration
    // Path    -    messages/messages.yml
    // Purpose -    To keep messages in a separate file so when the main configuration is replaced due to update,
    //              message formats already customized can be maintained.

    // Gets messages configuration and returns as a FileConfiguration object.
    public FileConfiguration getMsgConfig() {
        return this.msgConfig;
    }

    // Loads 'messages/messages.yml' to the server.
    public void loadMsgConfig() {
        msgFile = new File(plugin.getDataFolder() + File.separator + "messages", "messages.yml");

        // Checks to see if 'messages/messages.yml' exists.
        if(!msgFile.exists()) {
            msgFile.getParentFile().mkdirs();
            plugin.saveResource("messages" + File.separator + "messages.yml", false);
            plugin.console.sendMsg("i", "Messages configuration not found, creating 'messages/messages.yml'");
        } else {
            plugin.console.sendMsg("i", "Messages configuration found, loading 'messages/messages.yml'");
        }

        // Sets variable 'msgConfig' as a YamlConfiguration and loads it.
        msgConfig = new YamlConfiguration();
        try {
            msgConfig.load(msgFile);
        } catch(IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public FileConfiguration getMOTDConfig() {
        return this.motdConfig;
    }

    // Loads 'messages/motd.yml' to the server.
    public void loadMOTDConfig() {
        motdFile = new File(plugin.getDataFolder() + File.separator + "messages", "motd.yml");

        // Checks to see if 'messages/motd.yml' exists.
        if(!motdFile.exists()) {
            motdFile.getParentFile().mkdirs();
            plugin.saveResource("messages" + File.separator + "motd.yml", false);
            plugin.console.sendMsg("i", "MOTD configuration not found, creating 'messages/motd.yml'");
        } else {
            plugin.console.sendMsg("i", "MOTD configuration found, loading 'messages/motd.yml'");
        }

        // Sets variable 'motdConfig' as a YamlConfiguration and loads it.
        motdConfig = new YamlConfiguration();
        try {
            motdConfig.load(motdFile);
        } catch(IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    // Gets database configuration and returns as a FileConfiguration object.
    public FileConfiguration getDBConfig() {
        return this.dbConfig;
    }

    // Loads 'database/mongoDB.yml' to the server.
    public void loadDBConfig() {
        // Gets main plugin configuration to utilize database_type value.

        // Checks to see what database tpye is listed in the 'config.yml' to allow to future expansion and use
        // of alternate database types.
        if (plugin.config.getPluginConfig().getString("database_type").equalsIgnoreCase("mongodb")) {
            dbFile = new File(plugin.getDataFolder() + File.separator + "database", "mongodb.yml");

            // Checks to see if 'database/mongoDB.yml' exists.
            if (!dbFile.exists()) {
                dbFile.getParentFile().mkdirs();
                plugin.saveResource("database" + File.separator + "mongodb.yml", false);
                plugin.console.sendMsg("i", "MongoDB configuration not found, creating 'database/mongodb.yml'");
            } else {
                plugin.console.sendMsg("i", "MongoDB configuration found, loading 'database/mongodb.yml'");
            }

            // Sets variable 'dbConfig' as YamlConfiguration and loads it.
            dbConfig = new YamlConfiguration();
            try {
                plugin.config.getDBConfig().load(dbFile);
            } catch (IOException | InvalidConfigurationException e) {
                e.printStackTrace();
            }


            // Checks to verify there are no null values.
            String uri = plugin.config.getDBConfig().getString("uri");
            String database = plugin.config.getDBConfig().getString("database");
            String player_data = plugin.config.getDBConfig().getString("player_data_collection");

            if (uri.isEmpty() | database.isEmpty() | player_data.isEmpty()) {
                plugin.console.sendMsg("e", "There can not be any empty values in 'mongodb.yml'");
                plugin.getServer().getPluginManager().disablePlugin(plugin);
            }
        } else {
            plugin.console.sendMsg("e", "Invalid database type in 'config.yml'");
            plugin.getServer().getPluginManager().disablePlugin(plugin);

        }
    }
}
