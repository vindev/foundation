package us.vindere.util;

import org.bukkit.Bukkit;
import us.vindere.core.core;

public class cons {
    // Instance plugin.
    private core plugin;
    public cons(core pl){
        plugin = pl;
    }

    // Sends console messages with a predetermined format depending on whether the message is specified as info, warning, error, or test message.
    public void sendMsg(String type, String msg) {
        if (type == "i") {
            Bukkit.getConsoleSender().sendMessage(plugin.color.codes("[CORE] (&bINFO&r) " + msg));
        } else if (type == "w") {
            Bukkit.getConsoleSender().sendMessage(plugin.color.codes("[CORE] (&eWARN&r) " + msg));
        } else if (type == "e") {
            Bukkit.getConsoleSender().sendMessage(plugin.color.codes("[CORE] (&cERR&r) " + msg));
        } else if (type == "t") {
            Bukkit.getConsoleSender().sendMessage(plugin.color.codes("[CORE] (&dTEST&r) " + msg));
        }
    }
}
