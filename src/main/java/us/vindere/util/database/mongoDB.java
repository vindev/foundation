package us.vindere.util.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bukkit.configuration.file.FileConfiguration;
import us.vindere.core.core;

public class mongoDB {
    // Instance plugin.

    private core plugin;
    public mongoDB(core pl) {
        plugin = pl;
    }

    public MongoCollection<Document> getPlayerCollection() {
        FileConfiguration dbFile = plugin.config.getDBConfig();

        String mongodb_uri = dbFile.getString("uri");
        String mongodb_db = dbFile.getString("database");
        String mongodb_playercollection = dbFile.getString("player_data_collection");

        MongoClientURI conn = new MongoClientURI(mongodb_uri);
        MongoClient client = new MongoClient(conn);
        MongoDatabase db = client.getDatabase(mongodb_db);
        MongoCollection<Document> playerCollection =  db.getCollection(mongodb_playercollection);
        return playerCollection;
    }
}
