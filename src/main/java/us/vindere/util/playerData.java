package us.vindere.util;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import us.vindere.core.core;

public class playerData {
    private String uuid;
    private String user;
    private String display;
    private int bal;
    private String ip;
    private String first_login;
    private String last_login;

    // Instance plugin.
    private core plugin;
    public playerData(core pl){
        plugin = pl;
    }

    public Object[] fromUUID(String uuid) {
        if (plugin.config.getPluginConfig().getString("database_type").equalsIgnoreCase("mongodb")) {
            MongoCollection<Document> playerCollection = plugin.playerCollection;
            Document playerRecord = new Document("uuid", uuid);
            Document playerFound = playerCollection.find(playerRecord).first();

            this.uuid = uuid;
            this.user = playerFound.getString("player");
            this.display = playerFound.getString("display_name");
            this.bal = playerFound.getInteger("balance");
            this.ip = playerFound.getString("ip");
            this.first_login = playerFound.getString("first_login");
            this.last_login = playerFound.getString("last_login");

            return new Object[] {
                    this.uuid, this.user, this.display, this.bal, this.ip, this.first_login, this.last_login
            };
        }
        return null;
    }

    public Object[] fromString(String user) {
        if (plugin.config.getPluginConfig().getString("database_type").equalsIgnoreCase("mongodb")) {
            MongoCollection<Document> playerCollection = plugin.playerCollection;
            Document playerRecord = new Document("user", user);
            Document playerFound = playerCollection.find(playerRecord).first();

            if (playerFound == null){
                return null;
            }

            this.uuid = playerFound.getString("uuid");
            this.user = user;
            this.display = playerFound.getString("display_name");
            this.bal = playerFound.getInteger("balance");
            this.ip = playerFound.getString("ip");
            this.first_login = playerFound.getString("first_login");
            this.last_login = playerFound.getString("last_login");

            return new Object[] {
                    this.uuid, this.user, this.display, this.bal, this.ip, this.first_login, this.last_login
            };
        }
        return null;
    }

    public void setString (String uuid, String player, String field, String data){
        if (plugin.config.getPluginConfig().getString("database_type").equalsIgnoreCase("mongodb")) {
            Document playerRecord;
            Document playerFound;
            Player playerObject = Bukkit.getPlayer(uuid);
            MongoCollection<Document> playerCollection = plugin.playerCollection;

            if (playerObject != null) {
                playerRecord = new Document("uuid", uuid);
                playerFound = playerCollection.find(playerRecord).first();
            } else {
                playerRecord = new Document("user", player);
                playerFound =  playerCollection.find(playerRecord).first();
            }

            Bson updateField =  new Document(field, data);
            Bson updateOperation = new Document("$set", updateField);
            playerCollection.updateOne(playerFound, updateOperation);
        }
    }

    public void setInt (String uuid, String player, String field, int data){
        if (plugin.config.getPluginConfig().getString("database_type").equalsIgnoreCase("mongodb")) {
            MongoCollection<Document> playerCollection = plugin.playerCollection;
            Document playerRecord;
            Document playerFound;
            Player playerObject = Bukkit.getPlayer(uuid);

            if (playerObject != null) {
                playerRecord = new Document("uuid", uuid);
                playerFound = playerCollection.find(playerRecord).first();
            } else {
                playerRecord = new Document("user", player);
                playerFound = playerCollection.find(playerRecord).first();
            }

            Bson updateField =  new Document(field, data);
            Bson updateOperation = new Document("$set", updateField);
            playerCollection.updateOne(playerFound, updateOperation);
        }
    }

    public Document getDocument (Player player) {
        Document playerRecord = new Document("uuid", player.getUniqueId().toString());
        return plugin.mongodb.getPlayerCollection().find(playerRecord).first();
    }

    public Document getOfflineDocument (Player player) {
        return plugin.mongodb.getPlayerCollection().find(new Document ("user", player)).first();
    }
}
