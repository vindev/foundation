package us.vindere.util;

import us.vindere.core.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class date {
    // Instance plugin.
    private core plugin;
    public date(core pl){
        plugin = pl;
    }

    // Returns the current date either short-hand or long-hand.
    public static String getDate(String type){
        if (type == "short"){
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
            Date date = new Date();
            String dateString = format.format(date);
            return dateString;
        } else if (type == "full") {
            SimpleDateFormat format = new SimpleDateFormat("EEEEE, MMMMM d, yyyy HH:mm:ss Z");
            Date date = new Date();
            String dateString = format.format(date);
            return dateString;
        }
        return null;
    }

    // Returns the string given into a date in SimpleDateFormat either short-handed or long-handed.
    public static Date parseDate(String type, String dateString){
        if (type == "short"){
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
            Date date;
            try {
                date = format.parse(dateString);
                return date;
            } catch (ParseException e){
                e.printStackTrace();
            }
        } else if (type == "full") {
            SimpleDateFormat format = new SimpleDateFormat("EEEEE, MMMMM d, yyyy HH:mm:ss Z");
            Date date;
            try {
                date = format.parse(dateString);
                return date;
            } catch (ParseException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    // Returns the given date into a new format either short-handed or long handed.
    public static String reformatDate (String type, String old_format, String old_date) {
        if (type.equals("short")){
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
            SimpleDateFormat formatOld = new SimpleDateFormat(old_format);
            Date date;
            try {
                date = formatOld.parse(old_date);
                return format.format(date);
            } catch (ParseException e){
                e.printStackTrace();
            }
        } else if (type.equals("full")) {
            SimpleDateFormat format = new SimpleDateFormat("EEEEE, MMMMM d, yyyy HH:mm:ss Z");
            SimpleDateFormat formatOld = new SimpleDateFormat(old_format);
            Date date;
            try {
                date = formatOld.parse(old_date);
                return format.format(date);
            } catch (ParseException e){
                e.printStackTrace();
            }
        }
        return null;
    }
}
