package us.vindere.util;

import org.bukkit.ChatColor;
import us.vindere.core.core;

public class trans {
    // Instance plugin.
    private core plugin;
    public trans(core pl){
        plugin = pl;
    }

    // Convert standardised color codes.
    public String codes (String msg) {
        String msg2 = ChatColor.translateAlternateColorCodes('&', msg);
        return msg2;
    }
}
